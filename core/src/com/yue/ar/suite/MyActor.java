package com.yue.ar.suite;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by luokangyu on 2017/9/14.
 */

public class MyActor extends Actor {

    TextureRegion textureRegion;

    public MyActor(TextureRegion textureRegion){
        this.textureRegion = textureRegion;

    // 設定演員的寬高等於紋理的寬高
        setSize(this.textureRegion.getRegionWidth(),this.textureRegion.getRegionHeight());

    }


    public TextureRegion getRegin(){
        return textureRegion;
    }


    public void setRegion(TextureRegion region){
        this.textureRegion = region;

    // 重新設定 寬高
        setSize(this.textureRegion.getRegionWidth(),this.textureRegion.getRegionHeight());
    }

    /*
     * 演员的處理
     *
     * @param delta
     *      表示从渲染上一帧开始到现在渲染当前帧的时间间隔, 或者称为渲染的 时间步 / 时间差。单位: 秒
     */
    public void act(float delta){
        super.act(delta);

    }

    /**
     * 绘制演员
     *
     * @param batch
     *      纹理画布, 用于绘制演员封装的纹理。SpriteBatch 就是实现了 Batch 接口
     *
     * @param parentAlpha
     *      父节点的透明度, 处理透明度和演员的颜色属性有关, 稍微复杂, 这里暂时先不处理
     */

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        if(textureRegion == null || !isVisible()){
            return;
        }

        batch.draw(
                textureRegion,
                getX(), getY(),
                getOriginX(), getOriginY(),
                getWidth(), getHeight(),
                getScaleX(), getScaleY(),
                getRotation()
        );


    }
}
