package com.yue.ar.suite;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import javax.xml.soap.Text;

public class ARSuite extends ApplicationAdapter {


	SpriteBatch batch; // 畫布
	Texture img; // 貼圖
	private MyActor actor;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");

		actor = new MyActor(new TextureRegion(img));

		actor.setPosition(50, 100);

		actor.setOrigin(0,0);

		actor.setScale(0.5f,1.0f);

		actor.setRotation(-5);
	}

	@Override
	public void render () {
		// 黑色清屏
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// 更新演員邏輯
		actor.act(Gdx.graphics.getDeltaTime());

		batch.begin();
// 绘制演员（这里暂且先直接绘制, 位置相对于屏幕左下角）
		actor.draw(batch, 1.0F);


		batch.end();
	}
	
	@Override
	public void dispose () {
		// 当应用退出时释放资源
		if (batch != null) {
			batch.dispose();
		}
		// 应用退出, 纹理不在需要用到, 释放纹理资源
		if (img != null) {
			img.dispose();
		}
	}
}
